const express = require('express')
var path = require('path');
const { Client, MessageMedia } = require('whatsapp-web.js');
const qrcode = require('qrcode') //convert string qrcode to html qrcode
const fs = require('fs');
const socketIO = require('socket.io')
const http = require('http')
var path = require('path');
var logger = require('morgan');
const { body, validationResult } = require('express-validator')
const { phoneNumberFormatter } = require('./helpers/formatter')
// const fileUpload = require('express-fileupload')
const multer  = require('multer')
const upload = multer()
const axios = require('axios');
const { response } = require('express');

const app = express()
const server = http.createServer(app)
const io = socketIO(server)

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// used to upload file/media
// app.use(fileUpload({
//     debug: true
// }))

const SESSION_FILE_PATH = './session.json';
let sessionCfg;
// if session path not null
if (fs.existsSync(SESSION_FILE_PATH)) {
    // set sessionCfg same as value of SESSION_FILE_PATH
    sessionCfg = require(SESSION_FILE_PATH);
}

app.use(express.static(path.join(__dirname, 'public'))); //must use this to import script from views/index.html and other. if not use will error in import
app.get('/', (req, res) => {
    res.sendFile('views/index-1.html', { root: __dirname})
})

const client = new Client({ 
    puppeteer: { 
        headless: false,
        // avoid make new chromium instance each restart
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--no-first-run',
            '--no-zygote',
            '--single-process', // <- this one doesn't works in Windows
            '--disable-gpu'
          ],
    },
    session: sessionCfg
});

client.on('auth_failure', msg => {
    // Fired if session restore was unsuccessfull
    console.error('AUTHENTICATION FAILURE', msg);
});

client.on('message', msg => {
    if (msg.body == '!ping') {
        msg.reply('pong');
    }
});

client.initialize();

//socket io
// each user will representative as socket parameter
io.on('connection', function(socket){
    socket.emit('message', 'Menghubungkan.....')

    client.on('qr', (qr) => {
        // Generate and scan this code with your phone
        console.log('QR RECEIVED', qr);
        //to display qrcode in html. qr paramter is string not image. so need convert
        qrcode.toDataURL(qr, (err, url) => {
            socket.emit('qr', url)
            socket.emit('message', 'QR Code diterima, silahkan scan')
        })
    });

    client.on('ready', () => {
        socket.emit('message', 'Whatsapp telah siap')
        socket.emit('ready', 'Whatsapp telah siap')
    });

    // authenticate moved to socket, so after user authenticated not send notif
    // authenticate to user and then store authenticated to session
    client.on('authenticated', (session) => {
        socket.emit('authenticated', 'Whatsapp telah terotentikasi')
        socket.emit('message', 'Whatsapp telah terotentikasi')
        console.log('AUTHENTICATED', session);
        sessionCfg=session;
        fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
            if (err) {
                console.error(err);
            }
        });
    });
    
})

// check wheather the number insert is registered or not
let checkRegisteredNumber = async function(number){
    return await client.isRegisteredUser(number)
} 

// send text only
app.post('/send-message', [
    body('number').notEmpty(),
    body('text').notEmpty(),
], async (req, res) => {
    const errors = validationResult(req).formatWith(({msg}) => {
        return msg
    })

    if(!errors.isEmpty()){
        return res.status(422).json({
            status: false,
            message: errors.mapped() //error diformat mapped
        })
    }

    const number = phoneNumberFormatter(req.body.number)
    const text = req.body.text

    let isRegisteredNumber = await checkRegisteredNumber(number)
    if(!isRegisteredNumber){
        return res.status(422).json({
            status: false,
            message: "Nomor Hp tidak terdaftar di whatsapp"
        })
    }

    client.sendMessage(number, text)
    .then(data => {
        res.status(200).json({
            status: true,
            data: data
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            err: err
        })
    })
})

// send with media from path
app.post('/send-media-path', [
    body('number').notEmpty(),
], async (req, res) => {
    const errors = validationResult(req).formatWith(({msg}) => {
        return msg
    })

    if(!errors.isEmpty()){
        return res.status(422).json({
            status: false,
            message: errors.mapped() //error diformat mapped
        })
    }

    const number = phoneNumberFormatter(req.body.number)
    const caption = req.body.caption
    const media = MessageMedia.fromFilePath('./moonlight16.jpg')

    let isRegisteredNumber = await checkRegisteredNumber(number)
    if(!isRegisteredNumber){
        return res.status(422).json({
            status: false,
            message: "Nomor Hp tidak terdaftar di whatsapp"
        })
    }

    client.sendMessage(number, media, { caption: caption })
    .then(data => {
        res.status(200).json({
            status: true,
            data: data
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            err: err
        })
    })
})

// send with media upload
app.post('/send-media-upload', upload.single('file'), [
    body('number').notEmpty(),
], async (req, res) => {
    const errors = validationResult(req).formatWith(({msg}) => {
        return msg
    })

    if(!errors.isEmpty()){
        return res.status(422).json({
            status: false,
            message: errors.mapped() //error diformat mapped
        })
    }

    let number = phoneNumberFormatter(req.body.number)
    let caption = req.body.caption
    let file = req.file

    let media = new MessageMedia(file.mimetype, file.buffer.toString('base64'), file.originalname + "-" + Date.now())

    let isRegisteredNumber = await checkRegisteredNumber(number)
    if(!isRegisteredNumber){
        return res.status(422).json({
            status: false,
            message: "Nomor Hp tidak terdaftar di whatsapp"
        })
    }

    client.sendMessage(number, media, { caption: caption })
    .then(data => {
        res.status(200).json({
            status: true,
            data: data
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            err: err
        })
    })
})

// send with media url
app.post('/send-media-url', [
    body('number').notEmpty(),
], async (req, res) => {
    const errors = validationResult(req).formatWith(({msg}) => {
        return msg
    })

    if(!errors.isEmpty()){
        return res.status(422).json({
            status: false,
            message: errors.mapped() //error diformat mapped
        })
    }

    const number = phoneNumberFormatter(req.body.number)
    const caption = req.body.caption
    // const media = MessageMedia.fromFilePath('./moonlight16.jpg')
    // const file = req.files.file
    // const media = new MessageMedia(file.mimetype, file.data.toString('base64'), file.name)
    const fileUrl = req.body.file

    let isRegisteredNumber = await checkRegisteredNumber(number)
    if(!isRegisteredNumber){
        return res.status(422).json({
            status: false,
            message: "Nomor Hp tidak terdaftar di whatsapp"
        })
    }

    let mimetype
    let attachment = await axios.get(fileUrl, { responseType: 'arraybuffer' }).then(response => {
        mimetype = response.headers['content-type']
        return response.data.toString('base64')
    })
    const media = new MessageMedia(mimetype, attachment, 'namaFile')

    client.sendMessage(number, media, { caption: caption })
    .then(data => {
        res.status(200).json({
            status: true,
            data: data
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            err: err
        })
    })
})

let port = process.env.PORT || 5000;
server.listen(port, function(){
    console.log("Server running on port: " + port)
})
