const express = require('express')
var path = require('path');
const { Client, MessageMedia, NoAuth, LocalAuth, LegacySessionAuth } = require('whatsapp-web.js');
const qrcode = require('qrcode') //convert string qrcode to html qrcode
const fs = require('fs');
const socketIO = require('socket.io')
const http = require('http')
var path = require('path');
var logger = require('morgan');
const { body, validationResult, param } = require('express-validator')
const { phoneNumberFormatter } = require('./helpers/formatter')
const multer  = require('multer')
const upload = multer()
const axios = require('axios');
const { response, json } = require('express');

const app = express()
const server = http.createServer(app)
const io = socketIO(server)

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public'))); //must use this to import script from views/index.html and other. if not use will error in import
app.get('/', (req, res) => {
    res.sendFile('views/index-2.html', { root: __dirname})
})

let sessions = [] //store all user that login
let SESSIONS_FILE = './sessions.json'

let setSessionsFile = function(session){
    fs.writeFile(SESSIONS_FILE, JSON.stringify(session), function(err){
        if(err) console.log(err)
    })
}

let getSessionsFile = function(){
    // because SESSIONS_FILE data type is json, data must JSON.parse
    return JSON.parse(fs.readFileSync(SESSIONS_FILE))
}

const createSession = function(id, description, io){
    // const SESSION_FILE_PATH = `./session-${id}.json`;
    // let sessionCfg;
    // // if session path not null
    // if (fs.existsSync(SESSION_FILE_PATH)) {
    //     // set sessionCfg same as value of SESSION_FILE_PATH
    //     sessionCfg = require(SESSION_FILE_PATH);
    // }
    
    const client = new Client({
        restartOnAuthFail: true,
        puppeteer: { 
            headless: true,
            // avoid make new chromium instance each restart
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-dev-shm-usage',
                '--disable-accelerated-2d-canvas',
                '--no-first-run',
                '--no-zygote',
                '--single-process', // <- this one doesn't works in Windows
                '--disable-gpu'
              ],
        },
        // session: sessionCfg
        authStrategy: new NoAuth()
    });
    
    client.initialize();

    client.on('qr', (qr) => {
        // Generate and scan this code with your phone
        // console.log('QR RECEIVED', qr);
        //to display qrcode in html. qr paramter is string not image. so need convert
        qrcode.toDataURL(qr, (err, url) => {
            io.emit('qr', { id: id, src: url })
            io.emit('message', { id: id, text: 'QR Code diterima, silahkan scan' })
        })
    });

    client.on('ready', async () => {
        io.emit('message', { id: id, text: 'Whatsapp telah siap'})
        io.emit('ready', { id: id })

        // if user is ready(already scan before)
        let savedSessions = await getSessionsFile()
        let sessionsIndex = await savedSessions.findIndex(sess => {
            return sess.id == id
        })
        savedSessions[sessionsIndex].ready = true //set session index[x] on column ready to true
        setSessionsFile(savedSessions) //save to sessions.json
    });

    // authenticate to user and then store authenticated to session
    client.on('authenticated', (session) => {
        io.emit('authenticated', { id: id })
        io.emit('message', { id: id, text: 'Whatsapp telah terotentikasi'})
        // udah gak kepake karena pakai multiple device
        // sessionCfg=session;
        // fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
        //     if (err) {
        //         console.error(err);
        //     }
        // });
    });

    client.on('auth_failure', (session) => {
        // Fired if session restore was unsuccessfull
        io.emit('message', { id: id, text: 'Otentikasi gagal, restarting.....'})
    });

    // client logout/disconnected
    client.on('disconnected', (reason) => {
        io.emit('message', { id: id, text: 'Whatsapp terputus...'})
        // udah gak kepake karena pakai multiple device
        // fs.unlinkSync(SESSION_FILE_PATH, function(err){
        //     if(err) return console.log(err)
        //     console.log('Session file dihapus')
        // })

        client.destroy()
        client.initialize()

        // if user is disconnect, remove his file session
        let savedSessions = getSessionsFile()
        let sessionsIndex = savedSessions.findIndex(sess => {
            return sess.id == id
        })
        savedSessions.splice(sessionsIndex, 1) //remove session user index[x]
        setSessionsFile(savedSessions) //save to sessions.json

        // tell browser if user(x) already remove. so remove display for user(x)
        io.emit('remove-session', id)
    })

    // add client to session. So can use in outer function
    sessions.push({
        id: id,
        description: description,
        client: client
    })

    // insert session to file
    let savedSessions = getSessionsFile()
    let sessionsIndex = savedSessions.findIndex(sess => {
        return sess.id == id
    })
    if(sessionsIndex == -1){ //-1 mean not found
        savedSessions.push({
            id: id,
            description: description,
            ready: false
        })
        setSessionsFile(savedSessions)
    }

}

let init = function(socket){
    let savedSessions = getSessionsFile()
    if(savedSessions.length > 0){
        // if socket have value. value from init() or init(socket)
        if(socket){
            // send data init to FE
            socket.emit('init', savedSessions)
        } else {
            savedSessions.forEach(sess => {
                createSession(sess.id, sess.description, io)
            });
        }
        
    }
}
init() //not have parameter. 


//socket io
// each user will representative as socket parameter
io.on('connection', function(socket){
    // socket contain data, will send to browser via init(socket)
    init(socket)
    //listen from FE.
    socket.on('create-session', function(data){
        createSession(data.id, data.description, io)
    })
})

// send text only
app.post('/send-message/:sender', [
    param('sender').notEmpty(),
    body('receiver').notEmpty(),
    body('text').notEmpty(),
], async (req, res) => {
    try {
        let errors = validationResult(req).formatWith(({msg}) => {
            return msg
        })

        if(!errors.isEmpty()){
            return res.status(422).json({
                status: false,
                message: errors.mapped() //error diformat mapped
            })
        }

        let sender = req.params.sender
        let receiver = phoneNumberFormatter(req.body.receiver)
        let text = req.body.text

        // get client
        let client = await sessions.find(sess => sess.id == sender)
        if(!client){
            return res.status(404).json({
                status: false, 
                message: "Pengirim tidak aktif / ditemukan"
            })
        }

        // check wheather the insert receiver number is registered or not
        let checkRegisteredNumber = async function(receiver){
            return await client.client.isRegisteredUser(receiver)
        }

        let isRegisteredNumber = await checkRegisteredNumber(receiver)
        if(!isRegisteredNumber){
            return res.status(422).json({
                status: false, 
                message: "Nomor Hp penerima: " + receiver + " tidak terdaftar di whatsapp"
            })
        }

        let data = await client.client.sendMessage(receiver, text)
        res.status(200).json({ status: true, message: 'Berhasil mengirim pesan text', data: data })
    } catch (error) {
        console.log(error)
        return res.status(400).json({
            status: false, 
            error: error
        })
    }
})

// send with media upload
app.post('/send-media-upload/:sender', upload.single('file'), [
    param('sender').notEmpty(),
    body('receiver').notEmpty(),
], async (req, res) => {
    try {
        let errors = validationResult(req).formatWith(({msg}) => {
            return msg
        })
    
        if(!errors.isEmpty()){
            return res.status(422).json({
                status: false,
                message: errors.mapped() //error diformat mapped
            })
        }
    
        let sender = req.params.sender
        let receiver = phoneNumberFormatter(req.body.receiver)
        let caption = req.body.caption
        let file = req.file
    
        let media = new MessageMedia(file.mimetype, file.buffer.toString('base64'), file.originalname + "-" + Date.now())
    
        // get client
        let client = await sessions.find(sess => sess.id == sender)
        if(!client){
            return res.status(404).json({
                status: false, 
                message: "Pengirim tidak aktif / ditemukan"
            })
        }
    
        // check wheather the insert receiver number is registered or not
        let checkRegisteredNumber = async function(receiver){
            return await client.client.isRegisteredUser(receiver)
        }
    
        let isRegisteredNumber = await checkRegisteredNumber(receiver)
        if(!isRegisteredNumber){
            return res.status(422).json({
                status: false, 
                message: "Nomor Hp penerima: " + receiver + " tidak terdaftar di whatsapp"
            })
        }
    
        let data = await client.client.sendMessage(receiver, media, { caption: caption })
        res.status(200).json({ status: true, message: 'Berhasil mengirim pesan media', data: data })
    } catch (error) {
        res.status(400).json({
            status: false,
            error: error
        })
    }
})

// send with media url
app.post('/send-media-url/:sender', [
    param('sender').notEmpty(),
    body('receiver').notEmpty(),
], async (req, res) => {
    try {
        let errors = validationResult(req).formatWith(({msg}) => {
            return msg
        })
    
        if(!errors.isEmpty()){
            return res.status(422).json({
                status: false,
                message: errors.mapped() //error diformat mapped
            })
        }

        let sender = req.params.sender
        let receiver = phoneNumberFormatter(req.body.receiver)
        let caption = req.body.caption
        let fileUrl = req.body.file
    
        // get client
        let client = await sessions.find(sess => sess.id == sender)
        if(!client){
            return res.status(404).json({
                status: false, 
                message: "Pengirim tidak aktif / ditemukan"
            })
        }
    
        // check wheather the insert receiver number is registered or not
        let checkRegisteredNumber = async function(receiver){
            return await client.client.isRegisteredUser(receiver)
        }

        let isRegisteredNumber = await checkRegisteredNumber(receiver)
        if(!isRegisteredNumber){
            return res.status(422).json({
                status: false, 
                message: "Nomor Hp penerima: " + receiver + " tidak terdaftar di whatsapp"
            })
        }

        let mimetype
        let attachment = await axios.get(fileUrl, { responseType: 'arraybuffer' }).then(response => {
            mimetype = response.headers['content-type']
            return response.data.toString('base64')
        })

        let fileName =  'whatsapp-file' + "-" + Date.now()
        let media = new MessageMedia(mimetype, attachment, fileName)
    
        let data = await client.client.sendMessage(receiver, media, { caption: caption })
        res.status(200).json({ status: true, message: 'Berhasil mengirim pesan media', data: data })
    } catch (error) {
        console.log(error)
        res.status(400).json({
            status: false,
            error: error
        })
    }
})

const findGroupByName = async function(name) {
    const group = await client.getChats().then(chats => {
        return chats.find(chat => 
            chat.isGroup && chat.name.toLowerCase() == name.toLowerCase()
        );
    });
    return group;
}
  
// Send message to group. Can use chatID or group name
app.post('/send-group-message/:sender', [
    body('groupId').custom((value, { req }) => {
        if (!value && !req.body.name) {
            throw new Error('Invalid value, you can use `id` or `name`');
        }
        return true;
    }),
    param('sender').notEmpty(),
    body('text').notEmpty(),
], async (req, res) => {
    try {
        const errors = validationResult(req).formatWith(({ msg }) => {
            return msg;
        });

        if (!errors.isEmpty()) {
            return res.status(422).json({
                status: false,
                message: errors.mapped()
            });
        }

        let sender = req.params.sender
        let groupId = req.body.groupId;
        let groupName = req.body.groupName;
        let text = req.body.text;

        // get client who login WA
        let client = await sessions.find(sess => sess.id == sender)
        if(!client){
            return res.status(404).json({
                status: false,
                message: "Pengirim tidak aktif / ditemukan"
            })
        }

        // Find the group by name
        if (!groupId) {
            let group = await findGroupByName(groupName);
            if (!group) {
                return res.status(422).json({
                    status: false,
                    text: 'Group: ' + groupName + ' tidak ditemukan.'
                });
            }
            groupId = group.id._serialized;
        }

        let data = await client.client.sendMessage(groupId, text)
        res.status(200).json({ status: true, message: 'Berhasil mengirim pesan ke grup', data: data })
    } catch (error) {
        res.status(500).json({
            status: false,
            response: err
        });
    }
});

let port = process.env.PORT || 5000;
server.listen(port, function(){
    console.log("Server running on port: " + port)
})
